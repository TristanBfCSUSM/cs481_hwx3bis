﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Homework3_Bis
{
    public partial class BonusPage : ContentPage
    {
        public BonusPage()
        {
            InitializeComponent();
        }

        async void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            var rand = new Random();
            string result = await DisplayPromptAsync("Choose a number", "");
            RandomNumber.Text = rand.Next(1, 10).ToString();
            if (RandomNumber.Text == result)
                DisplayAlert("You win !!!", "GG you found the right number!", "Amazing!");
            else
                DisplayAlert("You loose", "Unfortunatelly you didn't find the right number", "ok :(");
        }
    }
}
