﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Homework3_Bis
{
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }

        //Function using OnAppear()
        void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            DisplayAlert("About page alert", "Thanks to use our application ! If you enjoy this application please review it", "Got it !");
        }

    }
}
