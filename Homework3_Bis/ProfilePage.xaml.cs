﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace Homework3_Bis
{
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();
        }
        //Function using OnDisappear
        void ContentPage_Disappearing(System.Object sender, System.EventArgs e)
        {
            DisplayAlert("Profile page alert", "Don't forget to update your information", "Ok");
        }

        //Function associated with button to change the age on profile
        private async void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            string result = await DisplayPromptAsync("Modify your age", "What's your age ?");
            var final_result = result + " years old";
            AgeProfile.Text = final_result;
        }
        //Function associated with button to change current status on profile
        private async void Button_Clicked2(System.Object sender, System.EventArgs e)
        {
            string result = await DisplayPromptAsync("Modify your status", "What's your current status ?");
            StatusProfile.Text = result;
        }
    }
}
